__author__ = 'Th3 S3cr3t Ag3nt'
import tweepy
import re
import argparse
import threading
import sys
import os
import time
import unicodedata

logo = '            .#.                                                 =               \n'\
 '              =#.     ..=                                   .##.                \n'\
 '               .#.=   ..#                                =.##.                  \n'\
 '                . #=#.#..#                           =#. #=..                   \n'\
 '                  ..##.#.##=                       =# ##...                     \n'\
 '                    ..######                     .####=..             .##..     \n'\
 '     ..               ..#####                   .#### ..      .==.##############\n'\
 '=###########=.           #####                .#####..       # ############ . #.\n'\
 ' ###===.#######.          =####.             .####=      .=##########=....=..##.\n'\
 '.. =......==#######=        ###=.          ..#### .     .########=..........### \n'\
 ' =..............#######     ..##=          ####.       #######=............####.\n'\
 '###..............=.######..   .=#=.        = . .      ######=.............=####.\n'\
 '###..................######     .#                   ######=..............#####.\n'\
 '=##=...................####=     .                .=#####.=###............####..\n'\
 ' ###...........#####.....####.                   .#####=.#######=.......=####=  \n'\
 ' ####=........###.###=....#####               .=######..###..###=.......=####   \n'\
 ' .###.........###.###=......####=            .######....=#######.......=#### .  \n'\
 '  ####.........#####.......########= .=. ..#########.=...######.......#####.    \n'\
 ' .=####=.........=.....=#####. .###############= =#####..............####=.     \n'\
 '   #####...........=.####=.       =====####==      .=#####.=......=#####.       \n'\
 '   .#####........=####. .                            ..=#####=...=####=         \n'\
 '    .######..=#####..                                    ..##########.          \n'\
 '      .##########=                                             #########=.      \n'\
 '         .####.                                                    =######.     \n'\
 '     .#####=.                                                                   \n'\
 '    =###                                                      TWISHER v0.1      \n'\
 ' .=#.                                                                           \n'

# Parse config file ##########################################################

#import secrets
from ConfigParser import SafeConfigParser

parser = SafeConfigParser()
found = parser.read('keys.cfg')

if not found:
    print "Configuration Error!: Configuration file not found."
    print
    print
    exit()

# check the config file ######################################################
for section in [ 'authorization_keys' ]:
    if not parser.has_section(section):
        print "Configuration Error!: Missing authorization_keys section in config file."
        print
        print
        exit()
    for candidate in [ 'access_token', 'access_token_secret', 'consumer_key', 'consumer_secret' ]:
        if not parser.has_option(section, candidate):
            print 'Configuration Error!: Missing %s in config file.' % (candidate)
            print
            print
            exit()

access_token = parser.get('authorization_keys', 'access_token')
access_token_secret = parser.get('authorization_keys', 'access_token_secret')
consumer_key = parser.get('authorization_keys', 'consumer_key')
consumer_secret = parser.get('authorization_keys', 'consumer_secret')

# Parse arguments ############################################################
parser = argparse.ArgumentParser(
    description='Build personalized dictionary from a twitter feed.'
    )
parser.add_argument("ScreenName", help="Twitter handle of target.")
parser.add_argument("-l", "--lowercase", help="Convert all words to lower case.",
                    action="store_true")
parser.add_argument("-s", "--sort", help="Sort words alphabetically.",
                    action="store_true")
parser.add_argument("-a", "--alphanumeric", help="Only use alphanumeric characters.",
                    action="store_true")
parser.add_argument("-q", "--quiet", help="Hide logo.",
                    action="store_true")
parser.add_argument("-qq", "--veryquiet", help="Very little output to screen.",
                    action="store_true")
args = parser.parse_args()

if not args.quiet:
    print logo
else:
    print "TWISHER v0.1"

if not args.quiet:
    if args.lowercase:
        print("Lowercase turned on")
    if args.sort:
        print("Sort turned on")
    if args.alphanumeric:
        print("Alphanumeric turned on")

ScreenName=args.ScreenName


if not args.quiet:
    print "Retrieving data for " + ScreenName + "..."

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

twts = [];

for status in tweepy.Cursor(api.user_timeline, screen_name=ScreenName).items():
    twts.append(status);

user = api.get_user(screen_name=ScreenName)

data=[]

if not args.quiet:
    print "Analyzing data for " + ScreenName + "..."

if args.lowercase:
    data.append(user.screen_name.lower())
else:
    data.append(user.screen_name)

for word in user.name.split():
    if args.lowercase:
        data.append(word.lower())
    else:
        data.append(word)

for word in re.sub(r'\W+', ' ', user.description).split():
    if args.lowercase:
        data.append(word.lower())
    else:
        data.append(word)
for word in re.sub(r'\W+', ' ', user.location).split():
    if args.lowercase:
        data.append(word.lower())
    else:
        data.append(word)

for twt in twts:
    if args.alphanumeric:
        for word in re.sub(r'\W+', ' ', twt.text).split():
            if args.lowercase:
                data.append(word.lower())
            else:
                data.append(word)
    else:
        for word in twt.text.split():
            if args.lowercase:
                data.append(word.lower())
            else:
                data.append(word)

s = ".";
seq = ("statuses", user.screen_name, "txt"); # This is sequence of strings.

file = open(s.join( seq ), "w")

collection = set(data)
if args.sort:
    for word in sorted(collection):
       #print ".",
       file.write(word.encode('utf-8'))
       file.write("\n")
else:
    for word in collection:
       #print ".",
       file.write(word.encode('utf-8'))
       file.write("\n")

file.close()

if not args.quiet:
    print len(collection), "words added to dictionary", s.join(seq)
    print 
